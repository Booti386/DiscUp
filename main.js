(() => {

let main = () => {

let cfg = {
	/* The number of messages loaded in a row */
	msg_chunk_size: 100,
	/* The maximum number of messages on the screen */
	max_msg_count: 500,
	/* New messages are loaded as soon as the visible message area is closer to the edge than msg_load_treshold * view height. */
	msg_load_treshold: 0.10,
	img_max_width: 500,
	img_max_height: 500
};

let ctx = {};

ctx.storage = new DBStorage('DiscUp://' + window.location.pathname);
ctx.cur_channel = null;
ctx.is_loading_msgs = false;
ctx.last_displayed_msg_idx = 0;
ctx.first_displayed_msg_idx = -1;
ctx.auto_display_msg_interval_id = -1;

function fetch_json_object_promise(name)
{
	return ctx.storage.getItem(name)
		.then((data) => {
			if (data === null)
				return Promise.reject(data);
			return data;
		}).catch((err) => {
			return new APIRequest('GET', 'data/' + name + '.json', null)
				.setResponseType('json')
				.send()
				.then((req) => {
					let obj = req.response;
					if (obj === null)
						return Promise.reject(obj);

					ctx.storage.setItem(name, obj);
					return obj;
				});
		});
}

function hide_elem(elem)
{
	elem.classList.add('hidden');
}

function show_elem(elem)
{
	elem.classList.remove('hidden');
}

function scroll_to_elem(elem, parent)
{
	let rect = elem.getBoundingClientRect();
	let rectp = parent.getBoundingClientRect();

	if (rect.top < rectp.top)
		parent.scrollTop -= (rectp.top - rect.top);
	else if (rect.bottom > rectp.bottom)
		parent.scrollTop += (rect.bottom - rectp.bottom);
}

function format_text(text)
{
	return text.replace(/(["&<>])/g, (whole, html_special_char) => {
		if (typeof html_special_char !== 'undefined')
		{
			const rpl = {
				'"': '&quot;',
				'&': '&amp;',
				'<': '&lt;',
				'>': '&gt;'
			};

			return rpl[html_special_char];
		}
	});
}

let load_stor_info = ctx.storage.getItem('info').catch(() => null);
let load_xhr_info = new APIRequest('GET', 'data/info.json', null)
		.setResponseType('json')
		.send()
		.then((req) => req.response)
		.catch(() => null);

let load_info = Promise.all([load_stor_info, load_xhr_info])
		.then((values) => {
			let data_stor = values[0];
			let data_xhr = values[1];

			if (data_xhr === null)
				return;

			if (data_stor === null
					|| data_stor.uid !== data_xhr.uid
					|| data_stor.guild_id != data_xhr.guild_id)
			{
				ctx.storage.clear();
				ctx.storage.setItem('info', data_xhr);
			}
		});

let load_guild = load_info.then(() => fetch_json_object_promise('guild'));
let load_users = load_info.then(() => fetch_json_object_promise('users'));

load_guild.then((guild) => {
	let elem_channels = document.getElementById('channels');
	let elem_channels_loading = document.getElementById('channels_loading');
	let channels_html = '';

	if (typeof guild.system_channel_id === 'undefined'
			|| guild.system_channel_id === null)
		guild.system_channel_id = guild.id;

	guild.channels = guild.channels.map((channel) => {
		if (typeof channel.parent_id === 'undefined')
			channel.parent_id = null;
		return channel;
	}).sort((a, b) => {
		/* Display orphan channels before categories */
		if (a.position === b.position)
			return a.type - b.type;
		return a.position - b.position;
	});

	guild.members = guild.members.map((member) => {
		if (typeof member.nick === 'undefined')
			member.nick = null;
		return member;
	});

	channels_html += '<form>';

	let top_channels = guild.channels.filter((channel) => channel.parent_id === null);
	for (let channel of top_channels)
	{
		if (channel.type !== 0 && channel.type !== 4)
			continue;

		let channel_dom_id = 'channel-' + channel.id;

		if (channel.type === 4)
		{
			channels_html += '<li class="category">';

			channels_html += '<input id="' + channel_dom_id + '" type="checkbox" hidden checked></input>';
			channels_html += '<label for="' + channel_dom_id + '">'
					+ '<div class="category_label unselectable">'
						+ '<span class="list_expand">+</span><span class="list_reduce">-</span> ' + format_text(channel.name.toUpperCase())
					+ '</div>'
				+ '</label>';

			let sub_channels = guild.channels
					.filter((sub_channel) => sub_channel.parent_id === channel.id && sub_channel.type === 0)
					.forEach((sub_channel) => {
						let sub_channel_dom_id = 'channel-' + sub_channel.id;
						channels_html += '<ul>'
								+ '<input class="channel_checkbox" id="' + sub_channel_dom_id + '" type="radio" name="channels" hidden></input>'
								+ '<label for="' + sub_channel_dom_id + '">'
									+ '<div class="channel_label unselectable"># ' + format_text(sub_channel.name) + '</div>'
								+ '</label>'
							+ '</ul>';
					});

			channels_html += '</li>';
		}
		else if (channel.type === 0)
			channels_html += '<div>'
					+ '<input class="channel_checkbox" id="' + channel_dom_id + '" type="radio" name="channels" hidden></input>'
					+ '<label for="' + channel_dom_id + '">'
						+ '<div class="channel_label unselectable"># ' + format_text(channel.name) + '</div>'
					+ '</label>'
				+ '</div>';
	}

	channels_html += '</form>';

	elem_channels.innerHTML = channels_html;
	hide_elem(elem_channels_loading);
}, (err) => {
	let elem_channels_loading = document.getElementById('channels_loading');
	elem_channels_loading.innerHTML = 'Failed';
});

load_users.then((users) => {
	let elem_users_loading = document.getElementById('users_loading');
	hide_elem(elem_users_loading);
}, (err) => {
	let elem_users_loading = document.getElementById('users_loading');
	elem_users_loading.innerHTML = 'Failed';
});

function format_msg_content(guild, users, msg_content)
{
	const uri_regex = ""
		/* scheme */
		/* + "[A-Za-z][A-Za-z0-9.-]*:" */ /* Subset */
		+ "(?:https?|s?ftp):"
		/* hier-part */
		+ "(?:"
			/* path */
				/* top */
				+ "(?:"
					/* authority */
					+ "//"
						/* user_info */
						+ "(?:(?:[A-Za-z0-9._~!$&'()*+,;=:-]|%[A-Fa-f0-9]{2})*@)?"
						/* host */
						+ "(?:"
							/* ip_literal */
							+ "(?:\\[[A-Fa-f0-9]{4}(?::[A-Fa-f0-9]{4}){7}\\])"
							/* ipv4_addr */
							+ "|(?:25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9])(?:\\.(?:25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9])){3}"
							/* reg_name */
							+ "|(?:[A-Za-z0-9._~!$&'()*+,;=-]|%[A-Fa-f0-9]{2})*"
						+ ")"
						/* port */
						+ "(?::[0-9]*)?"
					/* absolute|rootless */
					/* + "|/?(?:[A-Za-z0-9._~!$&'()*+,;=:@-]|%[A-Fa-f0-9]{2})+" */ /* Disabled */
				+ ")"
				/* sub */
				+ "(?:/|[A-Za-z0-9._~!$&'()*+,;=:@-]|%[A-Fa-f0-9]{2})*"
			/* path absolute - no segment */
			/* + '|/' */ /* Disabled */
		+ ")?"
		/* query */
		+ "(?:\\?(?:[A-Za-z0-9._~!$&'()*+,;=:@/?-]|%[A-Fa-f0-9]{2})*)?"
		/* fragment */
		+ "(?:#(?:[A-Za-z0-9._~!$&'()*+,;=:@/?-]|%[A-Fa-f0-9]{2})*)?";

	const regex = new RegExp("\\\\([^A-Za-z\\d\\s])|<?(" + uri_regex + ")>?|<(a?:\\w+:\\d+)>|(@everyone)|(@here)|<@!?(\\d+)>|<@&(\\d+)>|<#(\\d+)>|\\*\\*\\*([\\s\\S]+?)\\*\\*\\*|\\*\\*([\\s\\S]+?)\\*\\*|\\*(\\S|\\S[\\s\\S]*?\\S)\\*|~~([\\s\\S]+?)~~|__([\\s\\S]+?)__|_([\\s\\S]+?)_|```([\\s\\S]+?)```|`([\\s\\S]+?)`|([\"&<>])", "g");

	return msg_content.replace(regex, (whole, escaped_char, uri, emoji, mention_everyone, mention_here, mention_user_id, mention_role_id, mention_channel_id, bold_italic_text, bold_text, italic_text, strikeout_text, underline_text, italic_text_alt, code_multiline, code_inline, html_special_char) => {
		if (typeof escaped_char !== 'undefined')
		{
			return escaped_char;
		}

		if (typeof uri !== 'undefined')
		{
			let uri_html = '<a href="' + uri + '" rel="noreferrer noopener" target="_blank">' + uri + '</a>';
			let uri_delim_st = whole.startsWith('<');
			let uri_delim_end = whole.endsWith('>');

			if (!uri_delim_st || !uri_delim_end)
				uri_html = (uri_delim_st ? '&lt;' : '') + uri_html + (uri_delim_end ? '&gt;' : '');
			return uri_html;
		}

		if (typeof mention_user_id !== 'undefined')
		{
			let mention_html = '<span class="message_mention message_mention_user">';

			if (typeof users[mention_user_id] === 'undefined')
				mention_html += format_text(whole);
			else
				mention_html += '@' + format_text(users[mention_user_id].nick);

			mention_html += '</span>'
			return mention_html;
		}

		if (typeof emoji !== 'undefined')
		{
			let emoji_split = emoji.split(':');
			let animated = emoji_split[0] === 'a';
			let name = emoji_split[1];
			let id = emoji_split[2];

			return '<img class="message_emoji" alt=":' + format_text(name) + ':" src="data/emojis/' + format_text(id) + '.' + (animated ? 'gif' : 'png') + '">';
		}

		if (typeof mention_everyone !== 'undefined')
		{
			let mention_html = '<span class="message_mention">'
				+ '@everyone'
			+ '</span>';
			return mention_html;
		}

		if (typeof mention_here !== 'undefined')
		{
			let mention_html = '<span class="message_mention">'
				+ '@here'
			+ '</span>';
			return mention_html;
		}

		if (typeof mention_role_id !== 'undefined')
		{
			let mention_html = '<span class="message_mention message_mention_role" data-role-id="' + mention_role_id + '">';
			let mention_role = guild.roles.find((role) => role.id === mention_role_id);

			if (typeof mention_role === 'undefined')
				mention_html += format_text(whole);
			else
				mention_html += '@' + format_text(mention_role.name);

			mention_html += '</span>'
			return mention_html;
		}

		if (typeof mention_channel_id !== 'undefined')
		{
			let mention_html = '<span class="message_mention message_mention_channel" data-channel-id="' + mention_channel_id + '">';
			let mention_channel = guild.channels.find((channel) => channel.id === mention_channel_id);

			if (typeof mention_channel === 'undefined')
				mention_html += format_text(whole);
			else
				mention_html += '#' + format_text(mention_channel.name);

			mention_html += '</span>'
			return mention_html;
		}

		if (typeof bold_italic_text !== 'undefined')
		{
			return '<span class="bold italic">'
				+ format_msg_content(guild, users, bold_italic_text)
			+ '</span>';
		}

		if (typeof bold_text !== 'undefined')
		{
			let bold_html = '<span class="bold">';
			bold_html += format_msg_content(guild, users, bold_text);
			bold_html += '</span>'
			return bold_html;
		}

		if (typeof italic_text_alt !== 'undefined')
			italic_text = italic_text_alt;

		if (typeof italic_text !== 'undefined')
		{
			let italic_html = '<span class="italic">';
			italic_html += format_msg_content(guild, users, italic_text);
			italic_html += '</span>'
			return italic_html;
		}

		if (typeof strikeout_text !== 'undefined')
		{
			let strikeout_html = '<span class="strikeout">';
			strikeout_html += format_msg_content(guild, users, strikeout_text);
			strikeout_html += '</span>'
			return strikeout_html;
		}

		if (typeof underline_text !== 'undefined')
		{
			let underline_html = '<span class="underline">';
			underline_html += format_msg_content(guild, users, underline_text);
			underline_html += '</span>'
			return underline_html;
		}

		if (typeof code_multiline !== 'undefined')
		{
			return '<pre class="message_code">'
				+ '<code>'
					+ code_multiline.trim()
				+ '</code>'
			+ '</pre>';
		}

		if (typeof code_inline !== 'undefined')
		{
			return '<code class="message_code">'
				+ code_inline.trim()
			+ '</code>';
		}

		if (typeof html_special_char !== 'undefined')
		{
			const rpl = {
				'"': '&quot;',
				'&': '&amp;',
				'<': '&lt;',
				'>': '&gt;'
			};

			return rpl[html_special_char];
		}
	});
}

function filename_get_ext(filename)
{
	let idx = filename.lastIndexOf(".");
	if (idx < 0)
		return '';
	return filename.substring(idx + 1);
}

function markup_from_media_filename(filename)
{
	const img_exts = ['png', 'gif', 'jpg', 'jpeg'];
	const video_exts = ['avi', 'mkv', 'mov', 'mp4', 'mpeg', 'mpg', 'webm'];

	let ext = filename_get_ext(filename).toLowerCase();
	if (img_exts.includes(ext))
		return 'img';
	if (video_exts.includes(ext))
		return 'video';
	return null;
}

function format_msg_attachments(msg)
{
	if (typeof msg.attachments === 'undefined'
			|| msg.attachments === null
			|| msg.attachments.length <= 0)
		return '';

	let msg_attachments_html = '<div class="message_attachments">';

	msg_attachments_html = msg.attachments.reduce((accum, attachment) => {
		let attachment_url_parts = attachment.proxy_url.match(/^https?:\/\/(?:[a-z0-9-]+\.)*discordapp\.(?:com|net)\/attachments\/([0-9]+)\/([0-9]+)\/(.+)$/);
		if (attachment_url_parts === null
				|| attachment_url_parts[3] !== attachment.filename)
			return accum;

		let attachment_url = 'data/attachments/' + attachment_url_parts[1] + '/' + attachment_url_parts[2] + '/' + attachment_url_parts[3];

		let msg_attachment_obj_html_inner = '';
		let msg_attachment_obj_html_outter = '';

		if (typeof attachment.width !== 'undefined' && attachment.width > 0)
		{
			let markup = markup_from_media_filename(attachment.filename);
			if (markup !== null)
			{
				let img_w = attachment.width;
				let img_h = attachment.height;
				let img_scale = 1;
				let img_scale_w = cfg.img_max_width / img_w;
				let img_scale_h = cfg.img_max_height / img_h;

				img_scale = Math.min(img_scale, img_scale_w, img_scale_h);

				img_w = (img_scale * img_w) | 0;
				img_h = (img_scale * img_h) | 0;

				let attrs = markup === 'img' ? '' : 'controls';
				let msg_attachment_obj_html = '<' + markup + ' ' + attrs + ' class="message_attachment_object" width="' + img_w + '" height="' + img_h + '" src="' + attachment_url + '">';
				if (markup === 'img')
					msg_attachment_obj_html_inner = msg_attachment_obj_html;
				else
					msg_attachment_obj_html_outter = msg_attachment_obj_html;
			}
		}

		let msg_attachment_html = '<div class="message_attachment_container">'
			+ '<div class="message_attachment">'
				+ '<a href="' + attachment_url + '" rel="noreferrer noopener" target="_blank">'
					+ '<span>⭳ ' + format_text(attachment.filename) + '</span>'
					+ msg_attachment_obj_html_inner
				+ '</a>'
				+ msg_attachment_obj_html_outter
			+ '</div>'
		+ '</div>';

		return accum + msg_attachment_html;
	}, msg_attachments_html);

	msg_attachments_html += '</div>'
	return msg_attachments_html;
}

function create_msg_chunk_elem(guild, users, messages, first_msg_idx, last_msg_idx)
{
	let elem_msg_chunk = document.createElement('div');
	elem_msg_chunk.className = 'message_chunk';
	elem_msg_chunk.dataset.firstMsgIdx = first_msg_idx;
	elem_msg_chunk.dataset.lastMsgIdx = last_msg_idx;

	messages = messages.slice(last_msg_idx, first_msg_idx + 1);

	let msgs_html = messages.reduceRight((accum, msg, msg_idx) => {
		let msg_html = '';

		/* Messages are stored in reverse order: messages[messages.length - 1] is the first message (the older) */
		const is_first_msg = msg_idx === messages.length - 1;
		const is_last_msg = msg_idx === 0;
		const prev_msg = !is_first_msg ? messages[msg_idx + 1] : null;
		const next_msg = !is_last_msg ? messages[msg_idx - 1] : null;

		if (is_first_msg
				|| msg.author.id !== prev_msg.author.id)
			msg_html += '<div class="message_group">';

		let date = new Date(msg.timestamp)
		let date_d = `${date.getDate()}`.padStart(2, '0');
		let date_mo = `${date.getMonth() + 1}`.padStart(2, '0');
		let date_y = `${date.getFullYear()}`.substr(-2, 2).padStart(2, '0');
		let date_dmy = `${date_d}/${date_mo}/${date_y}`

		let date_h = `${date.getHours()}`.padStart(2, '0');
		let date_mi = `${date.getMinutes()}`.padStart(2, '0');
		let date_s = `${date.getSeconds()}`.padStart(2, '0');
		let date_hms = `${date_h}:${date_mi}:${date_s}`

		msg_html += '<div class="message_container">'
			+ '<div class="message_content">'
				+ '<div class="message_date">'
					+ '<span>' + format_text(date_dmy) + '</span>'
					+ '<span> </span>'
					+ '<span>' + format_text(date_hms) + '</span>'
					+ '<span> </span>'
				+ '</div>'
				+ '<div class="message_data">'
					+ '<span class="message_author">' + format_text(users[msg.author.id].nick) + '</span>'
					+ '<span> </span>'
					+ '<span class="message_text">' + format_msg_content(guild, users, msg.content) + '</span>'
				+ '</div>'
			+ '</div>'
			+ format_msg_attachments(msg)
		+ '</div>';

		if (is_last_msg
				|| msg.author.id !== next_msg.author.id)
			msg_html += '<hr class="message_group_separator"></div>';

		return accum + msg_html;
	}, '');

	elem_msg_chunk.innerHTML = msgs_html;

	Array.from(elem_msg_chunk.getElementsByClassName("message_mention_channel")).forEach((elem_message_mention_channel) => {
		elem_message_mention_channel.addEventListener('click', (evt) => {
			let mention_channel_id = elem_message_mention_channel.dataset.channelId;
			if (typeof mention_channel_id === 'undefined')
				return;

			let elem_mention_channel = document.getElementById('channel-' + mention_channel_id);
			if (elem_mention_channel === null)
				return;

			elem_mention_channel.click();
		});
	});

	return elem_msg_chunk;
}

function undisplay_msg_chunks(first_msg_idx, last_msg_idx)
{
	let elem_messages = document.getElementById('messages');
	let elems_message_chunk = Array.from(elem_messages.getElementsByClassName('message_chunk'));

	elems_message_chunk.filter((elem) => elem.dataset.firstMsgIdx > first_msg_idx || elem.dataset.lastMsgIdx < last_msg_idx)
		.forEach((elem) => {
			elem.parentNode.removeChild(elem);
		});
}

function display_prev_msg_chunk(guild, users, messages)
{
	if (ctx.first_displayed_msg_idx >= messages.length - 1)
		return;

	let start_idx = ctx.first_displayed_msg_idx + cfg.msg_chunk_size;
	if (start_idx > messages.length - 1)
		start_idx = messages.length - 1;

	let msg_count = start_idx - ctx.last_displayed_msg_idx;
	if (msg_count > cfg.max_msg_count)
		msg_count = cfg.max_msg_count;
	let end_idx = start_idx - msg_count;

	undisplay_msg_chunks(start_idx, end_idx);
	ctx.last_displayed_msg_idx = end_idx;

	let elem_msg_chunk = create_msg_chunk_elem(guild, users, messages, start_idx, ctx.first_displayed_msg_idx + 1);
	ctx.first_displayed_msg_idx = start_idx;

	let elem_messages = document.getElementById('messages');
	elem_messages.insertBefore(elem_msg_chunk, elem_messages.firstElementChild);
}

function display_next_msg_chunk(guild, users, messages)
{
	if (ctx.last_displayed_msg_idx <= 0)
		return;

	let end_idx = ctx.last_displayed_msg_idx - cfg.msg_chunk_size;
	if (end_idx < 0)
		end_idx = 0;

	let msg_count = ctx.first_displayed_msg_idx - end_idx;
	if (msg_count > cfg.max_msg_count)
		msg_count = cfg.max_msg_count;
	let start_idx = end_idx + msg_count;

	undisplay_msg_chunks(start_idx, end_idx);
	ctx.first_displayed_msg_idx = start_idx;

	let elem_msg_chunk = create_msg_chunk_elem(guild, users, messages, ctx.last_displayed_msg_idx - 1, end_idx);
	ctx.last_displayed_msg_idx = end_idx;

	let elem_messages = document.getElementById('messages');
	elem_messages.appendChild(elem_msg_chunk);
}

Promise.all([load_guild, load_users]).then((values) => {
	let guild = values[0];
	let users = values[1];

	for (let user_idx in users)
	{
		if (!users.hasOwnProperty(user_idx))
			continue;

		let user = users[user_idx];

		let member = guild.members.find((member) => member.user.id === user.id);
		if (typeof member === 'undefined' || member.nick === null)
			user.nick = user.username;
		else
			user.nick = member.nick;
	}

	let elem_messages = document.getElementById('messages');
	let elem_messages_loading = document.getElementById('messages_loading');

	for (let channel of guild.channels)
	{
		if (channel.type !== 0)
			continue;

		let elem_channel = document.getElementById('channel-' + channel.id);
		if (elem_channel === null)
			continue;

		elem_channel.addEventListener('click', (evt) => {
			if (channel.id === ctx.cur_channel)
				return;

			let elem_channel_label = elem_channel.parentNode.querySelector('label[for="' + elem_channel.id + '"]');
			if (elem_channel_label !== null)
			{
				let elem_parent_channel = document.getElementById('channel-' + channel.parent_id);

				if (elem_parent_channel !== null && !elem_parent_channel.checked)
					elem_parent_channel.click();

				scroll_to_elem(elem_channel_label, document.getElementById('col_channels'));
			}

			show_elem(elem_messages_loading);
			elem_messages.innerHTML = '';
			ctx.cur_channel = channel.id;
			ctx.is_loading_msgs = true;
			ctx.last_displayed_msg_idx = 0;
			ctx.first_displayed_msg_idx = -1;

			let load_messages = fetch_json_object_promise('channel-' + channel.id);
			load_messages.then((messages) => {
				window.clearInterval(ctx.auto_display_msg_interval_id);

				if (messages.length <= 0)
				{
					hide_elem(elem_messages_loading);
					return;
				}

				ctx.auto_display_msg_interval_id = window.setInterval(() => {
					if (ctx.is_loading_msgs)
						return;

					if (elem_messages.firstElementChild === null)
					{
						display_prev_msg_chunk(guild, users, messages);
						elem_messages.scrollIntoView(false);
						hide_elem(elem_messages_loading);
						return;
					}

					let elem_col_messages = document.getElementById('col_messages');

					let is_underflow = ctx.first_displayed_msg_idx < messages.length - 1
							&& elem_col_messages.scrollTop <= (cfg.msg_load_treshold * elem_col_messages.clientHeight);
					let is_overflow = ctx.last_displayed_msg_idx > 0
							&& elem_col_messages.scrollHeight - (elem_col_messages.scrollTop + elem_col_messages.clientHeight) <= cfg.msg_load_treshold * elem_col_messages.clientHeight;

					if (!is_underflow && !is_overflow)
						return;

					let old_scroll = elem_col_messages.scrollTop;
					let elem_ref = is_underflow ? elem_messages.firstElementChild : elem_messages.lastElementChild;
					let elem_ref_old_offset = elem_ref.offsetTop;

					if (is_underflow)
						display_prev_msg_chunk(guild, users, messages);
					else
						display_next_msg_chunk(guild, users, messages);

					/* `1 +` to mitigate a scrolling bug in Firefox. */
					elem_col_messages.scrollTop = 1 + old_scroll + (elem_ref.offsetTop - elem_ref_old_offset);
				}, 100);

				ctx.is_loading_msgs = false;
			});
		});
	}

	let elem_system_channel = document.getElementById('channel-' + guild.system_channel_id);
	if (elem_system_channel !== null)
		elem_system_channel.click();
});

};

if (document.readyState === 'interactive'
		|| document.readyState === 'complete')
	window.setTimeout(main, 0);
else
	document.addEventListener('DOMContentLoaded', main);

})();
