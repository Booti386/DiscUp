function APIRequest(method, url, data)
{
	this.method = method;
	this.url = url;
	this.data = data;
	this.headers = {};
	this.mime = null;
	this.responseType = '';
	this.rq = null;
}

APIRequest.prototype.overrideMimeType = function(mime)
{
	if (this.rq !== null)
		return this;

	this.mime = mime;
	return this;
};

APIRequest.prototype.setHeader = function(k, v)
{
	if (this.rq !== null)
		return this;

	this.headers[k] = v;
	return this;
};

APIRequest.prototype.setResponseType = function(type)
{
	if (this.rq !== null)
		return this;

	this.responseType = type;
	return this;
};

APIRequest.prototype.send = function()
{
	if (this.rq !== null)
		return Promise.reject("Already sending.");

	this.rq = new XMLHttpRequest();

	return new Promise((resolve, reject) => {
		this.rq.onreadystatechange = (evt) => {
			if (this.rq.readyState !== XMLHttpRequest.DONE)
				return;

			if (((this.rq.status / 100) | 0) !== 2)
			{
				reject(this.rq);
				this.rq = null;
				return;
			}

			resolve(this.rq);
			this.rq = null;
		};

		this.rq.onerror = (evt) => {
			reject(this.rq);
			this.rq = null;
		};

		this.rq.open(this.method, this.url, true);

		for (let k in this.headers)
		{
			if (!this.headers.hasOwnProperty(k))
				continue;

			this.rq.setRequestHeader(k, this.headers[k]);
		}

		if (this.mime !== null)
			this.rq.overrideMimeType(this.mime);

		this.rq.responseType = this.responseType;

		this.rq.send(this.data);
	});
};
