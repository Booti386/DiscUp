function DBStorage(name)
{
	this.name = name;

	this.db = new Promise((resolve, reject) => {
		let open_db_rq = window.indexedDB.open(this.name, 1);
		open_db_rq.onerror = (evt) => {
			reject();
		};

		open_db_rq.onsuccess = (evt) => {
			let db = open_db_rq.result;
			resolve(db);
		};

		open_db_rq.onupgradeneeded = (evt) => {
			let db = open_db_rq.result;
			db.createObjectStore('root', { autoIncrement: true });
			return true;
		};
	});
}

DBStorage.emitRequest = function(instance, rq_name, result_expected)
{
	return instance.db.then((db) => {
		let args = Array.prototype.slice.call(arguments, 3);

		let transaction = db.transaction('root', 'readwrite');
		let store = transaction.objectStore('root');
		let rq = store[rq_name].apply(store, args);

		return new Promise((resolve, reject) => {
			transaction.onerror = (evt) => {
				reject();
			};

			rq.onerror = (evt) => {
				reject();
			};

			rq.onsuccess = (evt) => {
				if (!result_expected)
					return resolve();

				if (typeof rq.result === 'undefined')
					return rq.onerror(evt);

				resolve(rq.result);
			};
		});
	}, (err) => {
		return Promise.reject();
	});
};

DBStorage.prototype.key = function(i)
{
	throw 'Not implemented';
};

DBStorage.prototype.getItem = function(key_name)
{
	return DBStorage.emitRequest(this, 'get', true, key_name);
};

DBStorage.prototype.setItem = function(key_name, value)
{
	return DBStorage.emitRequest(this, 'put', true, value, key_name);
};

DBStorage.prototype.removeItem = function(key_name)
{
	return DBStorage.emitRequest(this, 'delete', false, key_name);
};

DBStorage.prototype.clear = function()
{
	return DBStorage.emitRequest(this, 'clear', false);
};
